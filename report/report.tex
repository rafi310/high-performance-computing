
\documentclass[a4paper,
11pt,
oneside,
openany,
parskip=half,				% Abstand nach Absatz (statt Einrückung)
headsepline=true,			% Linie nach Kopfzeile
footsepline=false,			% Linie vor Fußzeile
plainfootsepline=true,		% Linie vor Fußzeile auf \chapter{}-Seiten
listof=totoc,				% Abblidungs- und Tabellenverzeichnis im Imhalsverteichnis darstellen
toc=bibliography,			% Literaturverzeichnis im Inhlatsverzeichnis darstellen
abstract=off				% Abstract mit Titel
]{scrreprt}

\include{preamble}
%includes
%\makeglossaries
\include{glossaries}
\bibliography{biblio.bib}
% Book's title and subtitle
\title{\Huge \textbf{High Performance Computing with Python} \vspace{4mm} \\ \huge Final Report}
% Author
% \author{\textsc{First-name Last-name}\footnote{email address}}
\author{\textsc{Raphael Schneider} \\ \vspace{3mm}\text{5419163}  \\
\vspace{3mm}\text{raphael.schneider@students.uni-freiburg.de}}


\begin{document}

\makeatletter
    \begin{titlepage}
        \begin{center}
            \includegraphics[width=0.5\linewidth]{images/Uni_Logo-Grundversion_E1_A4_CMYK.eps}\\[4ex]
            {\huge \bfseries  \@title }\\[2ex] 
            {\LARGE  \@author}\\[20ex] 
            {\large \@date}
        \end{center}
    \end{titlepage}
\makeatother
\thispagestyle{empty}
\newpage

\tableofcontents
\newpage
%\printglossary[type=\acronymtype]
%\printglossary[type=\acronymtype]
%\printglossaries	


\chapter{Introduction} \label{cpt:introduction}
Physical simulations are used to solve real-world problems. The simulation attempts to replicate real-world processes on a laboratory scale \cite{physSimulation}. 
% https://www.gleeble.com/resources/what-is-physical-simulation.html

As an example, it is used to simulate the particle flow of a fluid in a grid. The advantage is that the different parameters which specify the fluid can be changed easily. But these simulations are very computationally intensive on a larger scale. Therefore \gls*{gls:hpc} is an essential part of physical simulations. 
\cite{symmetryArticle}
%https://www.symmetrymagazine.org/article/the-coevolution-of-particle-physics-and-computing

In recent years, the \gls*{gls:lbm} has developed into an alternative and promising numerical scheme for simulating fluid flows and modeling physics in a fluid. It is based on microscopic models and mesoscopic kinetic equations, which are calculated for each simulated timestep. One advantage of this method is that it focuses the average macroscopic behavior which makes the computations easier. Furthermore, this method can be implemented fully parallel. Therefore, code to solve this simulation can be divided across many distributed machines.\cite{annurevLBM}
%https://www.annualreviews.org/doi/10.1146/annurev.fluid.30.1.329

This report shows the implementation and results of the \gls*{gls:lbm} with the programming language \textit{python} (Version 3.8.10). The report structure is as follows.
\begin{description}
\item[Methods and Implementation]\hfill \\
 Explain the equations from the \gls*{gls:lbm} and show corresponding code snippets
\item[Simulation results]\hfill \\
Validate the implementation with figures and analyze how effective and fast the implementation is.
\item[Conclusion]\hfill \\
 Summarizes the findings, which have been obtained through the implementation.
\end{description}

\newpage
\chapter{Methods} %oder Lattice Boltzman Method als ueberschrift
 For the entire chapter, the explanations of the material made available online by Professors Greiner and Pastewka were used as the source.\cite{illias}
 
 The \gls*{gls:lbm} approximates the physical states of particles in a space, which is discretized in a lattice. For this approach, the discretization on the velocity space is D2Q9-model. This discretization model is shown in \autoref{fig:veloSpace}(a). \autoref{fig:veloSpace}(b) shows the movement of one particle within one time step along the discretized space.
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=10cm]{images/Gitter_LBM.png}
		\caption{discretization of the velocity space}
		\label{fig:veloSpace}
	\end{center}
\end{figure}

The whole \gls*{gls:lbm} can be broken down into several equations. 
\section{The Boltzmann Transport Equation}
First, we look for the equation for motion, the \gls*{gls:bte}. It consists of two parts, the streaming part and the collision part. The original Boltzmann transport equation (BTE) is given by the lecture:

\begin{equation}
\begin{aligned}
\pdv{f(\mathbf{r,v},t)}{t}+\mathbf{v}\nabla_r f(\mathbf{r,v},t)+a\nabla_vf(\mathbf{r,v},t) = C(f(\mathbf{r,v},t))
\end{aligned}
\label{eq:original_BTE}
\end{equation}

This equation has two main parts. On the left hand side of the equation is the streaming term. On the right hand side is the collision term $C(f)$.

The challenge with this equation is the probability density function $f(\mathbf{r,v},t)$. It has a dependency to the density $\rho(r,t)$ and the velocity $v$. That means the need to discretize  $f$ in space $r$, velocity $v$ and time $t$.

The velocity space can be discretized  by forcing the particle flow along the directions $c_i$ shown in \autoref{fig:veloSpace}(a). So the notation $f(r,v,t)$ can be substituted though  $f_i(r,t)$ , where the index \textit{i} refers to the directions. The variable $r$ stands for the x,y-position of a node in the lattice.

The velocity sets  $c_i$ (shown in \autoref{eq:d2q9-velocity}) and the discretization in space ($\Delta{x}$) and  in time ($\Delta{t}$) are chosen so that $c_i\Delta{t}$ points from the current node  $r$  to the neighboring ones ($r+c_i\Delta{t}$). This condition is fulfilled if unit values are chosen for $\Delta{x}$  and  $\Delta{t}$  and the following velocity set is chosen:
\begin{equation}
\begin{aligned}
c_i= 
\begin{bmatrix}
0 & 1 & 0 & -1 & 0 & 1 & -1 & -1 & 1 \\
0 & 0 & -1 & 0 & 1 & -1 & -1 & 1 & 1 \\
\end{bmatrix}^\top.
\end{aligned}
\label{eq:d2q9-velocity}
\end{equation}
After the discretization, this equation can be written in its discretized form. The corresponding equation is shown in \autoref{eq:discret_BTE} and is also known as the lattice Boltzmann equation:

\begin{equation}
\begin{aligned}
\underbrace{f_i(\mathbf{r}\ + \mathbf{c}_i\Delta t, t+\Delta t) = f_i(\mathbf{r}, t)}_{
	\text{streaming}
} &
\underbrace{+ C_i(\mathbf{r}, t)}_{
	\text{collision}
}
\end{aligned}
\label{eq:discret_BTE}
\end{equation}
\subsection{Streaming term}
If the collision part is considered to be zero, the result is a transport equation which represents the movement of particles in vacuum, where no interaction between the particles is considered. The result is that each particle will continue to travel in the vacuum according to Newton's second law.
\subsection{Collision term}
The collision term $C(f)$ represents the interactions between particles and is usually a complicated two particle scattering integral. To simplify this computation, we approximate this term by a relaxation time approximation. For this approximation the distribution function $f(\mathbf{r,v},t)$ have to relax locally to an equilibrium distribution $f^{eq}(\mathbf{r,v},t) $
With this assumption the discretized equation from \autoref{eq:discret_BTE} looks like this:

\begin{equation}
\begin{aligned}
f_i(\mathbf{r}\ + \mathbf{c}_i\Delta t, t+\Delta t) = f_i(\mathbf{r}, t)+ \omega(f_i^{eq}(\mathbf{r},t)-f_i(\mathbf{r},t))
\end{aligned}
\label{eq:discret_BTE_collision}
\end{equation}
Where $\omega =  \Delta t/ \tau$ is the relaxation parameter. This relaxation parameter defines later the viscosity of the fluid.

To calculate the local equilibrium $f_i^{eq}(\mathbf{r},t)$ one has to search for the local variables. In this approach, it is the local density $\rho(\mathbf{r})$ and the local average velocity $\mathbf{u(r)}$. In addition, the 9 channels of the D2Q9 lattice must be weighted as follows:

\begin{equation}
	\begin{aligned}
	\omega_i = \begin{bmatrix}
	\frac{4}{9}, \frac{1}{9}, \frac{1}{9}, \frac{1}{9}, \frac{1}{9}, \frac{1}{36}, \frac{1}{36}, \frac{1}{36}, \frac{1}{36}
	\end{bmatrix}
	\end{aligned}
	\label{eq:weighted_channels}
\end{equation}

So the final equation to calculate the local equilibrium is shown in \autoref{eq:local_equilibrium}.
\begin{equation}
f^{eq}_i(\rho(\mathbf{r}),\mathbf{u(r)}) = \omega_i \rho(r)\\
\biggl[1 + 3 \mathbf{c}_i \cdot \mathbf{u(r)}+ \frac{9}{2}(\mathbf{c}_i \cdot \mathbf{u(r)}^2 -\frac{3}{2} \| \mathbf{u(r)} \|^2
\biggr] 
\label{eq:local_equilibrium}
\end{equation}

With the streaming and collision operation, it is already possible to simulate the shear wave decay method. For this, the specific sinusoidal initial condition on the velocity or density field must be fulfilled. More to the shear wave decay is explained in \autoref{sec:shearWaveDecay}
%quelle Greiner
\section{Boundary handling}
Boundary conditions are an essential part of the \gls*{gls:lbm}. It describes how the streaming part takes place at the boundary. The main concept in the \gls*{gls:lbm} is to impose the conditions to the probability density function $f_i$.

There are two ways to implement the boundary conditions:
\begin{itemize}
	\item \textbf{dry-node}: boundaries are located on the link between nodes.
	\item \textbf{wet-node}: boundaries are placed on the lattice nodes
\end{itemize}
Since the implementation of dry-nodes is easier and they work just as well when placed exactly in the middle between two nodes, these are used. 

There are different boundaries for the \gls*{gls:lbm} that can be applied to the system:
\subsection{Periodic boundary conditions}
	It is a symmetric boundary condition where the fluid leaving a boundary re-enter the domain at the side opposite to the leaving side. This behavior is described with the following equation:
	\begin{equation}
		f_i(x_1,t)= f_i(x_N,t)
		\label{eq:boundary_periodic}	
	\end{equation}

	where $x_1$ is the first node in the physical domain and $x_N$ is the last.
\subsection{Rigid wall}
	A rigid wall is modelled by defining the bounce-back operation for the nodes on the computational domain.
	
	Suppose having a boundary on the lower side. There, all channels that go against the boundary must be transferred to the channels pointing upwards. This is done by assuming that the population of the downwards channel $f_7, f_4$ and $f_8$ which hit the wall at the time $t$, is perfectly bounced back within one time step $\Delta t$ to the upwards channel $f_6, f_2$ and $f_5$ .
	The behavior is for a boundary node $x_b$ is given by the following equation:
	\begin{equation}
		f_{\bar{i}}(x_b,t+\Delta t) = f_i^*(x_b,t)
		\label{eq:boundary_rigid}	
	\end{equation}
	Where the index $\bar{i}$ denotes the opposite channel of $i$.
\subsection{Moving wall}
	The moving wall is a modification of the rigid wall. The bouncing back populations will gain or lose momentum during the interaction with the wall. This gain or lose is proportional to the wall velocity $\mathbf{U_w}$. Only walls that move parallel to their direction are considered, since this condition ensures the mass conservation. The equation for a moving wall is given in \autoref{eq:boundary_moving}.
	\begin{equation}
	f_{\bar{i}}(x_b,t+\Delta t) = f_i^*(x_b,t)-2\omega_i \rho_{\omega} \frac{c_i \cdot u_{\omega}}{c_s^2}
	\label{eq:boundary_moving}	
	\end{equation}
	Again is the index $\bar{i}$ the opposite channel of $i$ and $f^*$ denotes the value of the probability density function before the streaming step.
	
	The scenario, in which the fluid flows between a fixed and a moving wall, is known as the \textbf{Couette flow}.
\subsection{Periodic boundary conditions with pressure gradient}
	This boundary condition describes a pressure variation $\Delta p$ between the inlet and the outlet of the lattice. So, this is also a symmetric boundary condition. It is the typical case of the flow in a pipe which presents the pressure difference between the inlet pressure $p_{in}$ and the outlet pressure $p_{out}$. To compute this condition, an extra layer of nodes outside the physical domain is needed. Therefore, because of the periodic condition the extra node at the inlet $\mathbf{x_0}$ corresponds to the last node at the outlet $\mathbf{x_N}$. As consequence, the node $\mathbf{x_1}$ is the image node from $\mathbf{x_{N+1}}$. The resulting equation to calculate this condition is seen in \autoref{eq:boundary_pressure}.
	\begin{equation}
	\begin{aligned}
	f_i^*(x_0,y,t) = f_i^{eq}(\rho_{in},\mathbf{u}_N) + (f_i^*(x_N,y,t)-f_i^{eq}(x_N,y,t))\\
	f_i^*(x_{N+1},y,t) = f_i^{eq}(\rho_{out},\mathbf{u}_1) + (f_i^*(x_1,y,t)-f_i^{eq}(x_1,y,t))
	\end{aligned}
	\label{eq:boundary_pressure}	
	\end{equation}
	with $\rho_{out} = p_{out}/c_s^2$ and $\rho_{in} = (p_{out}+\Delta p)/c_s^2$.
	
	The periodic boundary condition with pressure gradient has to be applied to the pre-streaming population. Contrarily all other boundary conditions are applied to the post-streaming population.
	
	The category of simulating the flow in a pipe which is driven by a pressure difference between the inlet and outlet is called \textbf{Hagen-Poiseuille flow}.

\section{Parallelization}
As mentioned in \autoref{cpt:introduction}, it is possible to fully parallelize the \gls*{gls:lbm}. Therefore the global lattice is divided into smaller grids of equal size. The size of the grids correlates with the number of processes available for calculation. In the end, each process is responsible for its own sub-lattice.

Because the lattice is smaller, the individual calculations are faster. However, to get a correct result, data must be communicated between the processes and an extra layer of nodes is needed. The data from other processes is copied into this extra layer or your own data is written into it for other processes.

The communication between the processes is illustrated in \autoref{fig:process_communication}. There, the communication to the right from rank 0 to rank 1 and the communication down between rank 2 and rank 5 is shown as an example.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=10cm]{images/communication.PNG}
		\caption{Exemplary communication between multiple ranks. }
		\label{fig:process_communication}
	\end{center}
\end{figure}

The \textit{Python \gls*{gls:mpi}} is used for the parallelization.

\chapter{Implementation}\label{cpt:Impl}
This chapter is about the implementation of the \gls*{gls:lbm} in \textit{python}. Basis of this implementation is the discretized lattice in D2Q9. 
%Contrary to expectation the \textit{y-axis} is chosen as the horizontal axis and the \textit{x-axis} is the vertical axis.

For the implementation are multiple libraries included. The code snippets mainly show two libraries. One is \textit{numpy} which is imported as \textit{np} and the \textit{mpi4py} library which is used for the parallel communication.
\section{Streaming}\label{sec:streaming}
The streaming is about how the particles in the lattice are shifted according to their velocity. This is achieved using the following simple python function shown in \autoref{lst:streaming}. The lgorithm uses the roll-function from the \textit{numpy} package, which implicit handle the periodic boundary condition. 
\begin{python}[label={lst:streaming}, caption={Streaming Function}]
def streaming_part():
   # iterate all channels for the shifts
   for idx in np.arange(NL):
     lattice_cij[idx] = np.roll(lattice_cij[idx, ...],
     velocities_ca[idx, ...], axis=(0, 1))
\end{python}
The postfixes \textit{cij} or \textit{ca} indicates the dimensions of the array. The letter $c$ stands for the count of channels (with the D2Q9 discretization there are 9 channels). $i$ represents for the y-dimension of the lattice and $j$ for the x-dimension.
The array \texttt{velocities\_ca} is the velocity set which is shown in \autoref{eq:d2q9-velocity}.
\section{Collision}
The implementation of the collision operation was split into two parts. First part is the calculation of the local equilibrium shown in \autoref{eq:local_equilibrium}.

\begin{python}[label={lst:local_equilibrium}, caption={Calculate the local equilibirum of a cell}]
def local_equilibrium()
    velo_dot_set = (velocity_aij.T @ velocities_ca.T).T
    velo_abs = np.linalg.norm(velocity_aij, axis=0) ** 2
    lattice_eq_cij = weights[..., np.newaxis, np.newaxis] *
      density_ij[np.newaxis, ...] * (1. + 3.*velo_dot_set+4.5 *
      velo_dot_set**2 - 1.5*velo_abs[np.newaxis, ...])
\end{python}
In the listening above (\autoref{lst:local_equilibrium}) the array \texttt{weights} represents the weighted channels introduced in \autoref{eq:weighted_channels}. The \texttt{np.newaxis} in the listening are necessary to multiply the arrays with different dimensions.

The second part of the collision operation consists of include the calculated equilibrium in the actual lattice. This is shown in \autoref{lst:apply_loc_eq}.
\begin{python}[label={lst:apply_loc_eq}, caption={Apply the local equilibrium to the actual lattice}]
def collision_part(self):
   local_equilibrium()
   lattice_cij = lattice_cij + 
     omega* (lattice_eq_cij - lattice_cij) 
\end{python}
There the parameter \texttt{omega} is the relaxation parameter.
\section{Borders}
As mentioned in \autoref{sec:streaming}, the \textbf{periodic boundary condition} is implicit done with the streaming operation. For the other three boundary conditions was a base-class implemented, from which the other boundary conditions can derive.

The rough structure of this base class is shown in \autoref{lst:base_boundary}:
\begin{python}[label={lst:base_boundary}, caption={Boundary-Base class from which the boundary conditions can derive}]
class BoundaryBase():
   def __init__(self, boundaryAlignment, lattice):
      self._boundary_alignment = boundaryAlignment   
   def __call__(self, lattice):
      self.handleBoundary(lattice)
   @abstractmethod
   def handleBoundary(self, lattice) :
      raise NotImplementedError
\end{python}
The \texttt{boundaryAlignment} indicates the location of the boundary (\texttt{RIGHT, TOP, LEFT, BELOW}). The property \texttt{lattice} is the reference to the \texttt{LatticeBoltzmanMethod-Class} which handles the whole lattice with the streaming and collision part.

Each boundary condition is therefore represented as a new class which derives from \texttt{BoundaryBase}. In these classes, the method that is called for each timestep is overridden.

For the \textbf{rigid wall}, the bounce-back operation was implemented.

The \textbf{moving wall} has an additional computation. The momentum of the moving wall (subtraction in \autoref{eq:boundary_moving}) can be precomputed in the init-function of the specific class. This can be done as the velocity stays the same and the wall-density don't change.

For the \textbf{periodic boundary condition with pressure gradient} is the equation from \ref{eq:boundary_pressure} straight forward implemented. Challenging was to adapt the equilibrium calculation to an array with less dimension than the local equilibrium calculation in the collision operator.

\section{Parallelization} \label{sec:imp:parallelization}
The \textit{mpi4py}-library is used for the parallelization. It provides python-bindings for the \glsreset{gls:mpi}\gls*{gls:mpi}, allowing Python applications to exploit multiple processors on workstations, clusters and supercomputers. For the \gls*{gls:lbm} with the \gls*{gls:mpi} a cluster with the desired count of ranks can be set up. \cite{mpiForPy}

%https://mpi4py.readthedocs.io/en/stable/
Each rank then holds general information about the global lattice. With this information a sub-lattice then be initialized which is calculated for each timestep.

The \texttt{Sendrecv}-method from the mpi4py-framework is used for the communication between the ranks. For the communication, it must be taken into account whether the rank is adjacent to a border. If the boundary is not periodic, no communication to other ranks is carried out in this direction. The implementation of is shown in \autoref{lst:parallel_communication}.
\begin{python}[label={lst:parallel_communication}, caption={Parallel communication between the single ranks in each direction}]
def CommunicateParallel():
	comm_manager = ParallelGridManager
	commDirec = comm_manager.commDirections
	# communicate right
	sendbuf = lattice_cij[:, :, -2].copy()
	comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf,recvbuf=
	  recvbuf[1], dest=commDirec[1], source=commDirec[0])
	if not comm_manager.isLeftBorder():
	   lattice_cij[:,:, 0] = recvbuf[1]
	# communicate left
	sendbuf = lattice_cij[:, : , 1].copy()
	comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf,recvbuf=
	  recvbuf[1], dest=commDirec[3],  source=commDirec[2])
	if not comm_manager.isRightBorder():
	   lattice_cij[:,:, -1] = recvbuf[1]
	# communicate up
	sendbuf = lattice_cij[:,1,:].copy()
	comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf,recvbuf=
	  recvbuf[0], dest=commDirec[5], source=commDirec[4])
	if not comm_manager.isBottomBorder():
	   lattice_cij[:, -1,:] = recvbuf[0]
	# communicate down
	sendbuf = lattice_cij[:, -2, : ].copy()
	comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf,recvbuf=
	  recvbuf[0], dest=commDirec[7], source=commDirec[6])
	if not comm_manager.isTopBorder():
	   lattice_cij[:, 0,:] = recvbuf[0]
\end{python}
The code snippet above describes the communication that is explained in \autoref{fig:process_communication}. The array \texttt{commDirec} contains the neighbors (above, below, left and right) of the current rank that have already been determined.

\chapter{Results} \label{cpt:results}
After discussing the implementation and other details this chapter illustrate the different simulations named in the previous chapters. The results of the simulations also serve the purpose of validating the written code by comparing the results with the analytic solution.

\section{Shear Wave decay}\label{sec:shearWaveDecay}
The shear wave decay method represents the time evaluation of a sinusoidal velocity perturbation in the lattice. The decay in the sinusoidal perturbation is measured over time and plotted.

First, there is the following initial condition for the density to obtain the sinusoidal perturbation: 
\begin{equation}
\begin{aligned}
\rho(r,0) = \rho_0 + \epsilon \sin(\frac{2\pi x}{X})
\end{aligned}
\label{eq:sinusoidalDens}
\end{equation}


\begin{figure}[!h]
	\centering
	\subfloat[][Density variation of the shear wave decay simulation after initialize the lattice with a sinusoidal density (\autoref{eq:sinusoidalDens}) ]{\includegraphics[width=0.4\linewidth]{images/ShearWaveDecay.png}\label{fig:densitySwd}}%
	\qquad
	\subfloat[][Velocity variation of the shear wave decay simulation. The different lines show the variation at different timesteps. The initialized lattice has the density one and a sinusoidal velocity profile like in \autoref{eq:sinusoidalVelo}. ]{\includegraphics[width=0.4\linewidth]{images/VelocityOverTime}\label{fig:velocitySwd}}%
	\caption{Shear wave decay with a sinusoidal perturbation in the flow. The dimension of the lattice was $100 \times 100$. Both figures show the y-axis cross section at $X/4$.}%
\end{figure}

Subfigure \ref{fig:densitySwd} represents the plot of the density field along the y-axis at the position $X/4$ where the initial value is the maximum because of the sinus profile. The amplitude of the sinusoidal oscillation is $\epsilon= 0.05$. Through the red dashed line, the analytic result is illustrated. This is calculated using the Navier-Stokes equation for incompressible fluids \cite{fei2018three} and looks like the following equation:
 
\begin{equation}
\begin{aligned}
u_x(r, t)= \epsilon \exp(-\nu\biggl(\frac{2\pi }{ Y}\biggl)^2 t) *\sin(\frac{2\pi y}{Y})
\end{aligned}
\label{eq:analyticSwd}
\end{equation}
The simulation shows that the measured values fit well to the analytic result. This can be used to validate that the streaming and collision parts are working correctly.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=9cm]{images/KinematicViscosity.png}
		\caption{Kinematic viscosity for different $\omega$ of the fluid. The measured viscosity with the shear wave decay is compared to the theoretical computed value.  }
		\label{fig:kin_visc}
	\end{center}
\end{figure}

Further, the whole lattice is initialized with the density one ($\rho(r,0)=1$) and a sinusoidal variation of the velocities in x-direction. The sinusoidal perturbation for the velocity is given by the following equation:
\begin{equation}
\begin{aligned}
u(x, t=0)= \begin{bmatrix}
\epsilon \sin(\frac{2\pi y}{Y}) \\
0 \\
\end{bmatrix}
\end{aligned}
\label{eq:sinusoidalVelo}
\end{equation}

Subfigure \ref{fig:velocitySwd} represents the time evolution of the velocity at a certain cross section. In contrast to the oscillating density over time, the velocity only decreases continuously to zero. This is caused by the viscosity, which is defined by the relaxation factor $\omega$.


\autoref{fig:kin_visc} reveals the relationship between omega and viscosity. For very small omegas and for $\omega >= 1.6$ the measured viscosity deviates from the theoretically expected viscosity.

\section{Couette Flow}
The Couette flow is the flow between two walls. One is fixed (rigid wall) and the other moves parallel with the velocity of $U_x$. 
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=10cm]{images/CouetteFlow.png}
		\caption{Couette flow simulation at different timesteps compared to the analytic result. Here the velocities are observed along the y-axis at x=0. The wall velocity $U_y$ and the relaxing factor $omega$ is 1. The domain is a $100 \times 100$ lattice  }
		\label{fig:couetteFlow}
	\end{center}
\end{figure}
\autoref{fig:couetteFlow} shows the result of this simulation. The moving wall is next to $y=0$. The plot shows the velocities along the y-axis at $x=0$. There it becomes visible that the moving wall slowly sets the fluid in motion, starting with the particles closest to the moving wall. Over time, this motion propagates along the entire y-axis. In the steady state (timestep $2^{15}$), the simulation can be validated with the analytical solution. This can be calculated using the following formula where $Y$ is the distance between the two walls.
\begin{equation}
u_x(y)=\frac{Y-y}{Y} *U_w
\label{eq:couette_analytic}	
\end{equation}
In \autoref{fig:couetteFlowStream} the streamplot of the Couette flow is shown at different timesteps. There you can see how the velocity field is slowly forming at the upper wall and growing further down from there. The color of the streamlines represents the velocity.
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=14cm]{images/StreamplotCouette.png}
		\caption{Streamplot of the Couette flow simulation at different timesteps. The velocity-value of the streamlines is indicated with the color. All simulation fundamentals are exactly the same as in \autoref{fig:couetteFlow}.}
		\label{fig:couetteFlowStream}
	\end{center}
\end{figure}

\newpage

\section{Poiseuille Flow}
This setting has two fixed walls at the top and bottom. The flow is caused from the pressure difference between the inlet (left border) and outlet (right border) $\Delta p$.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=10cm]{images/PoiseuilleFlow.png}
		\caption{Poiseuille flow simulation at different timesteps compared to the analytic result. Here the velocities are observed along the y-axis at the middle of the channel. The initial density difference between inlet and outlet is 2\%. The relaxing factor omega is $1.0$. The domain size is $500 \times 60$ }
		\label{fig:poiseuilleFlow}
	\end{center}
\end{figure}

In \autoref{fig:poiseuilleFlow} the velocity was again recorded along the y-axis, however not at the beginning but in the middle of the pipe (X=250). In this figure the velocity profile grows to a parabolic profile. It is possible to validate our simulation with the analytic solution derived from the Navier-Stokes equations. The following equation shows the analytic solution:
\begin{equation}
u_x(y)=-\frac{1}{2\mu} \frac{dp}{dx}y(h-y)
\label{eq:poiseuille_analytic}	
\end{equation}
where $h$ is the diameter of the pipe and $\mu= \rho \nu$ is the dynamic viscosity.

\begin{comment}
The \autoref{fig:poiseuilleFlow} shows a small difference between analytical and steady state solution. This difference is only visible at the maximum of the parabolic profile. It is caused by the fact that
\todo{diese Frage noch beantworten}
Warum ist analytische und steadystate Lösung der Dichte unterschiedlich. Achtung, keine Fangfrage, das darf so sein, aber warum ist es so? Überlegen Sie mal, was die treibende Kraft ist. Dichte und Druck sind proportional zueinander. Also ist der absolute Druck die treibenden Kraft? Oder anders gefragt, was ist denn an beiden Kurven (sprich Geraden) gleich?
\end{comment}

\autoref{fig:poiseuilleFlowStream} visualizes the evolution for different timesteps to the steady state. It is visible that the velocity field slowly begins to form at the inlet and outlet due to the pressure differences. In the end, the fastest speeds can be found in the middle of the pipe.
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=14cm]{images/StreamplotPoiseuille.png}
		\caption{Streamplot of the Poiseuille simulation at different timesteps. The velocity-value of the streamlines is indicated with the color. All simulation fundamentals are exactly the same as in \autoref{fig:poiseuilleFlow}. }
		\label{fig:poiseuilleFlowStream}
	\end{center}
\end{figure}
\newpage
\section{Lid-driven cavity }
The final simulation frame is a lid-driven cavity. To be more specific a sliding-lid, where three walls are fixed and one wall is moving with a constant speed $u_y$. This produces a flow in the fluid with characteristic vortexes as shown in the steady state in \autoref{fig:slidingLid}. The number which characterizes the vortexes is the Reynolds number which expresses the ratio between the inertial terms and the viscous ones:

\begin{equation}
Re=\frac{Lu}{\nu}
\label{eq:reynoldsnumber}	
\end{equation}
$L$ is the characteristic length of the body, $u$ is the flow velocity and $\nu$ the kinematic viscosity.

For the simulation I choose the Reynolds number to 1000. The initial values of the lattice nodes were chosen as $\rho(x)=1.0$ and $u(x)=(0,0)^T$. \autoref{fig:slidingLid} shows the streamlines in the steady state. The sliding lid features a large vortex in the center that rotates clockwise. In the lower corners there are two smaller vortexes that rotate in the opposite direction.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=14cm]{images/SlidingLidRe1000.png}
		\caption{Steady state of the Sliding lid experiment with Reynolds number 1000. The grid is quadratic with the size 300 and the wall velocity is 0,25. So the resulting relaxing parameter $\omega$ is 1.38}
		\label{fig:slidingLid}
	\end{center}
\end{figure}

\section{Scaling}
As already mentioned in \autoref{cpt:introduction}, these simulations are very computationally intensive. For this reason, the algorithm was parallelized via the \gls*{gls:mpi} framework. The implementation follows \autoref{sec:imp:parallelization} and each thread is bound to one processor. To demonstrate the scaling of the code, 100000 timesteps were calculated with a lattice of domain $300 \times 300$. The necessary resources are provided by the parallel computer system called \enquote{bwUniCluster 2.0+GFB-HPC} \cite{bwUniCluster}. For the scaling experiment the \enquote{Thin}-nodes of the cluster were used. These have a \textit{Intel Xeon Gold 6230} processors with 2.1 GHz \cite{bwUniClusterArchitecture}. The lattice was divided in rectangular sub regions, which each is assigned to one process.

The performance of the code is typically measured in \gls*{gls:mlups}. The \gls*{gls:mlups} are calculated with the following equation:

\begin{equation}
MLUPS=\frac{X*Y*timesteps *10^{-6}}{ time}
\label{eq:mlups}	
\end{equation}
The multiplication with the constant $10^{-6}$ is necessary to scale from lattice updates per second to million lattice updates per second. \autoref{fig:scaling} shows the result of the scaling experience.

There it can be seen that from 100 processes the scaling stagnates or drops slightly. This is because the inter-process communication between the processes is becoming too time-consuming and therefore does not lead to any improvement. The optimum is reached at around 100 processes, which corresponds to a sub-lattice domain of $30 \times 30$.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=10cm]{images/Scaling.png}
		\caption{Scaling of the algorithm on the bwUniCluster 2.0. It shows the \gls*{gls:mlups} in relation to the processes. The lattice has the domain $300 \times 300$. }
		\label{fig:scaling}
	\end{center}
\end{figure}
\newpage
\chapter{Conclusion}
The main goal of this paper was to implement the \gls*{gls:lbm} in python and to parallelize this simulation. For this purpose, the fundamental package \textit{numpy} was used, as well as \textit{mpi4py} for the use of the \gls*{gls:mpi}.

To implement the \gls*{gls:lbm} the velocity-space is discretized with the D2Q9-model. With this discretization, the \gls*{gls:bte} was split into two parts. On the one hand the streaming operation and on the other hand the collision operation. The corresponding implementations were also shown further on. For different simulations several boundary conditions were explained and implemented. Finally, the parallelization of the algorithm was discussed as well as the solution for the implementation.

Through various simulation frameworks, the function of each component of the method was demonstrated in \autoref{cpt:results}. For validation, shear wave decay, Couette flow and Poiseuille flow were used. For this purpose, analytical solutions were compared with the simulations. The result showed that the simulation and the analytic solution fit very well. Except if the relaxation term $\omega$ is chosen too large (>2).

With the lid-driven cavity, the influence of the Reynolds number is demonstrated. The larger it gets, the greater the noise in the field and the faster/more vortices are generated. With the help of this lid-driven cavity the scalability was also tested. Through the experiment, a speedup in execution time was measured compared to the serial implementation. It was also observed that this acceleration stalls or declines due to an increase in communication through many smaller domains. Also, the waiting time around the processes to synchronize harms the execution time.

\newpage
\printbibliography



\end{document}
