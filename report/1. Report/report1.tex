\documentclass[11pt]{article}
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%\pdfminorversion=4
	% NOTE: To produce blinded version, replace "0" with "1" below.
	\newcommand{\blind}{0}
	
	%%%%%%% IISE Transactions margin specifications %%%%%%%%%%%%%%%%%%%
	% DON'T change margins - should be 1 inch all around.
	\addtolength{\oddsidemargin}{-.5in}%
	\addtolength{\evensidemargin}{-.5in}%
	\addtolength{\textwidth}{1in}%
	\addtolength{\textheight}{1.3in}%
	\addtolength{\topmargin}{-.8in}%
    \makeatletter
    \renewcommand\section{\@startsection {section}{1}{\z@}%
                                       {-3.5ex \@plus -1ex \@minus -.2ex}%
                                       {2.3ex \@plus.2ex}%
                                       {\normalfont\fontfamily{phv}\fontsize{16}{19}\bfseries}}
    \renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                         {-3.25ex\@plus -1ex \@minus -.2ex}%
                                         {1.5ex \@plus .2ex}%
                                         {\normalfont\fontfamily{phv}\fontsize{14}{17}\bfseries}}
    \renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                        {-3.25ex\@plus -1ex \@minus -.2ex}%
                                         {1.5ex \@plus .2ex}%
                                         {\normalfont\normalsize\fontfamily{phv}\fontsize{14}{17}\selectfont}}
    \makeatother
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%%%%% IISE Transactions package list %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\usepackage{amsmath}
	\usepackage{graphicx}
	\usepackage{enumerate}
	\usepackage{xcolor}
	%\usepackage{natbib} %comment out if you do not have the package
	\usepackage{url} % not crucial - just used below for the URL
	%mypackages
	\usepackage{physics}
	\usepackage{biblatex}
	\usepackage{hyperref}
	\usepackage{todonotes}
	\usepackage{comment}
	\bibliography{IISE-Trans.bib}
	% Default fixed font does not support bold face
	\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{10} % for bold
	\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{10}  % for normal
	
	% Custom colors
	\usepackage{color}
	\definecolor{deepblue}{rgb}{0,0,0.5}
	\definecolor{deepred}{rgb}{0.6,0,0}
	\definecolor{deepgreen}{rgb}{0,0.5,0}
	
	\usepackage{listings}
	
	% Python style for highlighting
	\newcommand\pythonstyle{\lstset{
			language=Python,
			numbers=left,
			stepnumber=1,
			basicstyle=\ttm,
			morekeywords={self},              % Add keywords here
			keywordstyle=\ttb\color{deepblue},
			emph={MyClass,__init__},          % Custom highlighting
			emphstyle=\ttb\color{deepred},    % Custom highlighting style
			stringstyle=\color{deepgreen},
			frame=tb,                         % Any extra options here
			showstringspaces=true
			tabsize=2
	}}
	% Python environment
	\lstnewenvironment{python}[1][]
	{
		\pythonstyle
		\lstset{#1}
	}
	{}
	
	% Python for external files
	\newcommand\pythonexternal[2][]{{
			\pythonstyle
			\lstinputlisting[#1]{#2}}}
	
	% Python for inline
	\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}
%	\bibliographystyle{unsrtnat}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%%%%% Author package list and commands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%% Here are some examples %%%%%%%%%%%%%%
	%	\usepackage{amsfonts, amsthm, latexsym, amssymb}
	%	\usepackage{lineno}
	%	\newcommand{\mb}{\mathbf}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	\begin{document}
		
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\def\spacingset#1{\renewcommand{\baselinestretch}%
			{#1}\small\normalsize} \spacingset{1}
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		\if0\blind
		{
			\title{\bf First Report}
			\author{Raphael Schneider}
			\date{\today}
			\maketitle
		} \fi
		
		\bigskip

	%\newpage
	\spacingset{1.3} % DON'T change the spacing!


\section{Lattice Boltzmann Method} \label{s:lbm}
Fluid flows dynamics can easily simulated by the Lattice Boltzmann Method (LBM). The physical state is thus simulated at each time step.
The LBM approximates the physical states of particles in a space which is discretized in a lattice. For our approach the discretization on the velocity space is D2Q9 shown in \autoref{fig:veloSpace}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=10cm]{../logos/Gitter_LBM.png}
		\caption{discretization of the velocity space}
		\label{fig:veloSpace}
	\end{center}
\end{figure}

This LMB calculates a fluid density and velocity on a lattice with streaming and collision processes. For this first report we only look for these two processes and a variety of experiments and measurements with it. Complex boundary operations will be handled in the next reports. For this first approach, we assume that particles streaming against a border re-enter the lattice on the opposite.
The original Boltzmann transport equation (BTE) is given by the lecture:

\begin{equation}
\begin{aligned}
\pdv{f(\mathbf{r,v},t)}{t}+\mathbf{v}\nabla_r f(\mathbf{r,v},t)+a\nabla_vf(\mathbf{r,v},t) = C(f(\mathbf{r,v},t))
\end{aligned}
\label{eq:original_BTE}
\end{equation}

After the discretization we can write this equation in its discretized form (\autoref{eq:discret_BTE}):

\begin{equation}
\begin{aligned}
\underbrace{f_i(\mathbf{r}\ + \mathbf{c}_i\Delta t, t+\Delta t) = f_i(\mathbf{r}, t)}_{
	\text{streaming}
} &
\underbrace{+ C_i(\mathbf{r}, t)}_{
	\text{collision}
}
\end{aligned}
\label{eq:discret_BTE}
\end{equation}

\subsection{Streaming} \label{ss:streaming}
The streaming is about how the particles in the lattice are shifted according to their velocity. This is achieved using the following simple python function \autoref{code:streaming}. The Algorithm uses the roll-function from the \textit{numpy} package, which enable to handle the boundary condition automatically. The simple implementation is shown in the code sample \ref{code:streaming}

\begin{python}[label={code:streaming}, caption={Streaming Function}]
def streaming_part():
    # iterate all channels for the shifts
    for idx in np.arange(NL):
        lattice_cij[idx] = np.roll(lattice_cij[idx, ...], 
        velocities_ca[idx, ...], axis=(0, 1))
	
\end{python}

%erklären was mit lattice und den postfixen gemeint ist


\subsection{Collision} \label{ss:collision}
The Collision-process represents the interaction between particles. It is shown on the right-hand side of \autoref{eq:original_BTE}. To keep it simple we approximate it by a relaxation time approximation. For this approximation the distribution function $f(\mathbf{r,v},t)$ have to relax locally to an equilibrium distribution $f^{eq}(\mathbf{r,v},t) $
With this assumption the discretized equation from \autoref{eq:discret_BTE} looks like this:

\begin{equation}
\begin{aligned}
f_i(\mathbf{r}\ + \mathbf{c}_i\Delta t, t+\Delta t) = f_i(\mathbf{r}, t)+ \omega(f_i^{eq}(\mathbf{r},t)-f_i(\mathbf{r},t))
\end{aligned}
\label{eq:discret_BTE_collision}
\end{equation}
Where $\omega =  \Delta t/ \tau$ is the relaxation parameter.

The corresponding Python function for collision is therefore implemented as shown in the code sample \ref{code:collision}.
\begin{python}[label={code:collision}, caption={Streaming Function} ]
def collision_part():
    lattice_eq_cij = local_equilibrium()
    lattice_cij = lattice_cij + (lattice_eq_cij - lattice_cij) * omega
\end{python}
\newpage
\subsection{Results} \label{ss:result}

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=12cm]{../../figures/Milestone3/ShearWaveDecay 200 steps.png}
		\caption{Density from a certain lattice field while a simulation with 200 steps}
		\label{fig:200stepsDensity}
	\end{center}
\end{figure}

\newpage
\printbibliography
\spacingset{1}
	
\end{document}
