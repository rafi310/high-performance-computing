from source.algorithms.latticeBoltzman import LatticeBoltzmannMethod
from source.algorithms.ParallelCommunication import ParallelGridManager
import source.algorithms.BoundaryCondition as bc
import source.data.CommonDefinitions as cd
import argparse
import numpy as np
import time

"""
hier einmal mit command arguments alles definieren und dann die zeit nehmen
ausfuehren und danach nochmal die zeit nehmen und differenz berechnen
--> soll am Ende auf dem cluster ausgeführt werden
"""

def main () -> None:


    parser = argparse.ArgumentParser(prog='Sliding_Lid', usage='%(prog)s [options]')
    parser.add_argument('-X', type= int, required=True, help='x-dimension of the lattice.')
    parser.add_argument('-Y', type= int, required=True, help='y-dimension of the lattice.')
    parser.add_argument('-T', '--time_steps', type= int, required=True, help='Count of timesteps which should be executed.')
    parser.add_argument('--omega', type=float, help='relaxation factor omega.')
    parser.add_argument('-RE','--reynoldsnr', type= float, help='Raynoldsnumber for the experiment')
    parser.add_argument('-W', '--wall_velo', default=0.25, type=float, help='Velocity of the moving wall')
    parser.add_argument('-S', '--show_plot', default=False, type=bool, help='Should a plot been shown after the simulation')
    parser.add_argument('-P', '--exec_parallel', default=False, type=bool, help='Execute the sliding lid with the mpi-framework')
    args = parser.parse_args()
    
    animation_timesteps = np.concatenate((np.arange(0,1000, 10),
                                     np.arange(1000, 5000, 100),
                                     np.arange(5000, args.time_steps, 1000)))
    velocities = np.zeros((len(animation_timesteps+1),2, args.Y,args.X), dtype=np.float64)
    def callback(lattice: LatticeBoltzmannMethod, t: int) -> None:
        if t in animation_timesteps:
            velocities[np.where(animation_timesteps == t)] = lattice.getVelocity()
    assert args.omega is not None or args.reynoldsnr is not None
    if args.reynoldsnr is not None:
        args.omega = cd.get_omega_for_raynoldnumb(args.reynoldsnr, args.wall_velo, max(args.X, args.Y))
    
    density_field = np.ones((args.Y, args.X))
    velocity_field = np.zeros((2, args.Y,args.X))
    wall_speed = np.array([0,args.wall_velo])

    manager = ParallelGridManager(args.X, args.Y)
    if args.exec_parallel:
        args.X, args.Y = manager.local_buffer_shape
        density_field = np.ones((args.Y, args.X))
        velocity_field = np.zeros((2, args.Y,args.X))
        lattice = LatticeBoltzmannMethod(args.X, args.Y, omega=args.omega , init_density=density_field,
        init_velocity=velocity_field, init_plot=args.show_plot, parallel_comm=manager)
    else:
        lattice = LatticeBoltzmannMethod(args.X, args.Y, omega=args.omega , init_density=density_field, 
        init_velocity=velocity_field, init_plot=args.show_plot)
    boundary_list = []
    if args.exec_parallel:
        if manager.isLeftBorder():
            boundary_list.append(bc.RigidWall(cd.BoundaryAlignments.LEFT, lattice=lattice ))
        if manager.isRightBorder():
            boundary_list.append(bc.RigidWall(cd.BoundaryAlignments.RIGHT, lattice=lattice ))
        if manager.isBottomBorder():
            boundary_list.append(bc.RigidWall(cd.BoundaryAlignments.BOTTOM, lattice=lattice ))
        if manager.isTopBorder():
            boundary_list.append(bc.MovingWall(cd.BoundaryAlignments.TOP, lattice=lattice, wall_velo=wall_speed ))
    else:
        boundary_list.append(bc.RigidWall(cd.BoundaryAlignments.LEFT, lattice=lattice ))
        boundary_list.append(bc.RigidWall(cd.BoundaryAlignments.RIGHT, lattice=lattice ))
        boundary_list.append(bc.RigidWall(cd.BoundaryAlignments.BOTTOM, lattice=lattice ))
        boundary_list.append(bc.MovingWall(cd.BoundaryAlignments.TOP, lattice=lattice, wall_velo=wall_speed ))
    
    boundary_handling = bc.BoundaryHandlings(boundary_conditions=boundary_list)
    
    start = time.time()
    lattice.execute(timesteps=args.time_steps, boundary=boundary_handling)
    end = time.time()
    runtime = end-start
      
    Mlups = 300 * 300 * args.time_steps * 10e-6 / runtime
    if(manager.rank == 0):
      print("MLUPS: {}".format(Mlups))
      print("execution time: {} sec".format(round(runtime,3)))



if __name__ =="__main__":
    main()