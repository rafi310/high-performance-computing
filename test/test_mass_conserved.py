
import unittest
from source.algorithms.latticeBoltzman import LatticeBoltzmannMethod
import numpy as np

X, Y = 50, 50

class TestMassConserved(unittest.TestCase):
    def setUp(self) -> None:
        self._higher_density= 10
        self._test_density = np.ones((Y, X))*0.4
        self._test_density[:self._higher_density, :self._higher_density] = np.ones((self._higher_density,self._higher_density))



    def test(self) -> None:
        #set up lattice
        lattice_boltzmann = LatticeBoltzmannMethod(X, Y,omega=1, init_density=self._test_density)
        # get initial mass
        expected_mass = X*Y * 0.4 + self._higher_density **2 - self._higher_density**2*.4
        self.assertEqual(expected_mass, round(lattice_boltzmann.mass,2))
        # run simulation and check mass
        for i in range(500):
            lattice_boltzmann.boltzman_step()
            self.assertEqual(expected_mass,  round(lattice_boltzmann.mass,2))
        #do the test


# if __name__ == '__main__':
#     unittest.main()




