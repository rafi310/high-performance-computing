# High Performance Computing - Fluid Mechnics with Python

Report and final program for lecture *High Performance Computing - Fluid Mechnics with Python* in the summer semester 2022

The Python programm supports python version >= 3.8.10
## Getting started
Install all needed libraries for the repository
```cmd
pip install -r requirements.txt
```

Execute the sliding lid
```
python -m run_sliding_lid_exp.py
```
___
Or get help with
```
 python run_sliding_lid_exp.py -h
```


```
usage: Sliding_Lid [options]

optional arguments:
  -h, --help            show this help message and exit
  -X X                  x-dimension of the lattice.
  -Y Y                  y-dimension of the lattice.
  -T TIME_STEPS, --time_steps TIME_STEPS
                        Count of timesteps which should be executed.
  --omega OMEGA         relaxation factor omega.
  -RE REYNOLDSNR, --reynoldsnr REYNOLDSNR
                        Raynoldsnumber for the experiment
  -W WALL_VELO, --wall_velo WALL_VELO
                        Velocity of the moving wall
  -S SHOW_PLOT, --show_plot SHOW_PLOT
                        Should a plot been shown after the simulation
  -P EXEC_PARALLEL, --exec_parallel EXEC_PARALLEL
                        Execute the sliding lid with the mpi-framework
``` 
An example of the sliding lid is shown in <a href="./submission/slidingLidRe1000.mp4">submission/slidingLidRe1000</a>

## Milestones
The implementation of each Milestone is located in <a href="./source/milestones">source/milestones</a>

Each milestone is implemented as a jupyter notebook file (*.ipynb)
<!-- Execute milestones with bash command:
python -m source.milestones.milestoneX -->

## Scaling Tests
Use the `mpiexec` to run the sliding lid in parallel
```
mpiexec -n 16 python run_sliding_lid_exp -X 300 -Y 300 -T 100000 -RE 750 -P True

```

## Tests
Run all the test located in <a href="./test">/test</a> with 

```cmd
python -m unittest
```
## License
This project is private so noone should contribute to it


# set up cluster
copy lokal data to bwUniCluster
scp -r C:\Users\Raphael\Documents\Master_Projekte\high-performance-computing\source fr_rs532@uc2.scc.kit.edu:~/hpc

execute script with
sbatch hpc/scripts/