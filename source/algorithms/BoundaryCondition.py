
from abc import abstractmethod
from enum import IntEnum
import numpy as np
import source.data.CommonDefinitions as cd
from source.data.CommonDefinitions import BoundaryAlignments
from source.data.CommonDefinitions import ChannelInformation as ci
from typing import List
"""

kinds:
    - periodic (like the beginning)
        --> output at one bounary at time t is the input at the opposite boundary at time t+dt
    - rigid wall
        --> bounce back: each channel velocity at time t results in the anti-channel velocity at time t+dt
    - moving wall
        --> bounce back with a gain/loss of momentum: same as rigid wall just with a addition with the wall/density behaviours
    - inlet BC
        --> inlet nodes with certain velocity have to be equal to the equilibrium function (with density and velocity from inlet)
    - outlet BC
        --> channels that stream out this boundary got "lost", the other channels have to be computed by the nodes(idx-1) of the outlet node (idx)
        --> 2nd order accuracy the node at idx-2 also plays a role
    - periodic with pressure variation dp
        --> imagene points that relats to the points at the opposite. calculate it with the (non) equilibirum function and the inlet/outlet
        
        --> density lowers, velocity stays in the same parabolic skeem 
--> maybe it gives some common arguments/behaviour or sth like that

"""
LatticeBoltzmanMethodType = 'LatticeBoltzmannMethod'

class BoundaryBase():
    def __init__(self, boundaryAlignment: BoundaryAlignments, lattice: LatticeBoltzmanMethodType):
        self._boundary_alignment = boundaryAlignment
        self._lattice_shape = lattice.grid_shape

    def __call__(self, lattice) -> None:
        self.handleBoundary(lattice)

    @abstractmethod
    def handleBoundary(self, lattice: LatticeBoltzmanMethodType) -> None:
        """
        abstract method to handle the boundary current boundary condition 
        input: 
            lattice: reference to the lattice-boltzman object
        output: -
        """
        raise NotImplementedError


class BoundaryHandlings():
    def __init__(self, boundary_conditions: List[BoundaryBase]):
        self._boundaries = []

        if boundary_conditions is not None:
            for boundary_cond in boundary_conditions:
                self._boundaries.append(boundary_cond)

    def __call__(self, act_lattice: LatticeBoltzmanMethodType, pressure: bool = False) -> None:
        if len(self._boundaries) > 0:
            if pressure:
                for boundary in self._boundaries:
                    if isinstance(boundary, PeriodicWithPressure):
                        boundary(act_lattice)

            else:
                for boundary in self._boundaries:
                    if not isinstance(boundary, PeriodicWithPressure):
                        boundary(act_lattice)


class RigidWall(BoundaryBase):
    def __init__(self, boundaryAlignment: BoundaryAlignments, lattice: LatticeBoltzmanMethodType):
        super().__init__(boundaryAlignment, lattice=lattice)
        print("Rigid Wall at {}" .format(boundaryAlignment))

    def handleBoundary(self, lattice: LatticeBoltzmanMethodType) -> None:
        """
        bounce-back operation
        """
        lattice_pre = lattice.lattice_cij_pre
        lattice_post = lattice.getLattice()
        if self._boundary_alignment == BoundaryAlignments.TOP:
            input = ci.top_channels()
            output = ci.bottom_channels()
            lattice_post[output, 0,:] = lattice_pre[input, 0, :]
        elif self._boundary_alignment == BoundaryAlignments.BOTTOM:
            input = ci.bottom_channels()
            output = ci.top_channels()
            lattice_post[output, -1,:] = lattice_pre[input, -1, :]
        elif self._boundary_alignment == BoundaryAlignments.LEFT:
            input = ci.left_channels()
            output = ci.right_channels()
            lattice_post[output, :, 0] = lattice_pre[input, :, 0]
        elif self._boundary_alignment == BoundaryAlignments.RIGHT:
            input = ci.right_channels()
            output = ci.left_channels()
            lattice_post[output, :, -1] = lattice_pre[input, :, -1]
        else:
            print("something went wrong with rigid wall")
        # lattice.setLattice(lattice_post)


class  MovingWall(BoundaryBase):
    def __init__(self, boundaryAlignment: BoundaryAlignments, lattice: LatticeBoltzmanMethodType, wall_velo: np.ndarray):
        super().__init__(boundaryAlignment, lattice)
        print("Moving Wall at {}" .format(boundaryAlignment))
        self._wall_momentum = np.zeros((cd.NL, *lattice.getGridShape()), np.float32)
        self._wall_velo = wall_velo  # (2,1)
        self._wall_density = lattice._density_ij.mean()
        self.calcWallMomentum()
        

    def calcWallMomentum(self) -> np.ndarray:
        """
        precalculation of the momentum of moving walls if the velocity stays the same and the wall-density dont change
        2*w_i*ρ_w*(c_i⋅u_w/c_s**2)
        = 6*w_i*ρ_w*(c_i⋅u_w)
        # = scalar * (3,1) * scalar ( (3,2) @ (2,1))
        """
        if self._boundary_alignment == BoundaryAlignments.TOP:
            channels = ci.top_channels()
            one_field =  6 * cd.weights[channels] * self._wall_density * (cd.velocities_ca[channels] @ self._wall_velo)
            self._wall_momentum[channels, 0, :] = one_field[:, np.newaxis]
        elif self._boundary_alignment == BoundaryAlignments.BOTTOM:
            channels = ci.bottom_channels()
            one_field =  6 * cd.weights[channels] * self._wall_density * (cd.velocities_ca[channels] @ self._wall_velo)
            self._wall_momentum[channels, -1, :] = one_field[:, np.newaxis]
        elif self._boundary_alignment == BoundaryAlignments.LEFT:
            channels = ci.left_channels()
            one_field =  6 * cd.weights[channels] * self._wall_density * (cd.velocities_ca[channels] @ self._wall_velo)
            self._wall_momentum[channels, :, 0] = one_field[:, np.newaxis]
        elif self._boundary_alignment == BoundaryAlignments.RIGHT:
            channels = ci.right_channels()
            one_field =  6 * cd.weights[channels] * self._wall_density * (cd.velocities_ca[channels] @ self._wall_velo)
            self._wall_momentum[channels, :, -1] = one_field[:, np.newaxis]
        else:
            print("something went wrong with moving wall")

        self._init_finished = True

    def handleBoundary(self,  lattice: LatticeBoltzmanMethodType) -> None:
        lattice_pre = lattice.lattice_cij_pre
        lattice_post = lattice.getLattice()
        if self._boundary_alignment == BoundaryAlignments.TOP:
            input = ci.top_channels()
            output = ci.bottom_channels()
            lattice_post[output, 0,:] = lattice_pre[input, 0, :]- self._wall_momentum[ci.top_channels(), 0, :]
        elif self._boundary_alignment == BoundaryAlignments.BOTTOM:
            input = ci.bottom_channels()
            output = ci.top_channels()
            lattice_post[output, -1,:] = lattice_pre[input, -1, :] - self._wall_momentum[ci.bottom_channels(), -1, :]
        elif self._boundary_alignment == BoundaryAlignments.LEFT:
            input = ci.left_channels()
            output = ci.right_channels()
            lattice_post[output, :, 0] = lattice_pre[input, :, 0] -self._wall_momentum[ci.left_channels(), :, 0]
        elif self._boundary_alignment == BoundaryAlignments.RIGHT:
            input = ci.right_channels()
            output = ci.left_channels()
            lattice_post[output, :, -1] = lattice_pre[input, :, -1]- self._wall_momentum[ci.right_channels(), :, -1]
        else:
            print("something went wrong with moving wall")


class PeriodicWithPressure(BoundaryBase):
    """"
    just possible if there is the periodic boundary condition with pressure variation left and right (horizontal)
    or top and bottom (vertical)
    """
    class Orientation(IntEnum):
        HORIZONTAL = 0,
        VERTICAL = 1,
        NOT_DEFINED = 2
    def __init__(self, boundaryAlignment: BoundaryAlignments, boundaryAlignment2: BoundaryAlignments, lattice: LatticeBoltzmanMethodType, 
    density_inlet: float, density_outlet: float):
        super().__init__(boundaryAlignment, lattice)
        print("Pressurevariant Wall at {} and {}" .format(boundaryAlignment, boundaryAlignment2))
        self._boundary_alignment2 = boundaryAlignment2

        self._orientation = self.Orientation.NOT_DEFINED
        self._boundary_length = 0
        self._L = 0
        #add intEnum to get the orientation
        # LEFT = 0 + RIGHT = 2 -> 2 --> Horizonatl
        # TOP = 1 + BOTTOM = 3 -> 4 --> Vertical
        if(boundaryAlignment.value[0] + boundaryAlignment2.value[0] == 2 ):
            self._orientation = self.Orientation.HORIZONTAL
            # Y-Dimension
            self._boundary_length, self._L = lattice.getGridShape()
        if(boundaryAlignment.value[0] + boundaryAlignment2.value[0] == 4):
            self._orientation = self.Orientation.VERTICAL
            # X-Dimension
            self._L, self._boundary_length = lattice.getGridShape()
        assert self._orientation is not self.Orientation.NOT_DEFINED
        assert self._boundary_length > 0
        self._border_density_inlet = np.full(self._boundary_length, density_inlet)
        self._border_density_outlet = np.full(self._boundary_length, density_outlet)
        
    def local_equilibrium_boundary(self, velocity, density) -> np.ndarray:
        """
        calculate the local equilibirum from the in and outlet of the pressure difference
        input:
            velocity: row with velocity vectors
            density: row with density
        output:
            equilibium field of these rows
        """
        # (2, Y, X).T @ (9,2).T -> (X,Y,9).T
        velo_dot_set = (velocity.T @ cd.velocities_ca.T).T
        velo_abs = np.linalg.norm(velocity, axis=0) ** 2
        # (9,Y,X) = (9,1,1) * (1,Y,X) * (9, Y ,X)
        return cd.weights[..., np.newaxis] * density[np.newaxis, ...] *\
               (1. + 3.*velo_dot_set+4.5 * velo_dot_set**2 - 1.5*velo_abs[np.newaxis, ...])


    def handleBoundary(self, lattice: LatticeBoltzmanMethodType) -> None:
        lattice_pre = lattice.lattice_cij_pre
        lattice_post = lattice.getLattice()
        lattice_eq = lattice.lattice_eq_cij 
        if self._orientation == self.Orientation.HORIZONTAL:
            #inlet
            channels = ci.right_channels()
            # column -2 so we have one additional column outside the grid
            inlet_eq = self.local_equilibrium_boundary(lattice.getVelocity()[:,:,-2], self._border_density_inlet)
            lattice_post[channels, :, 0] = inlet_eq[channels, :] + (lattice_pre[channels, :, -2] - lattice_eq[channels, :, -2])
            
            #outlet
            channels = ci.left_channels()
            outlet_eq = self.local_equilibrium_boundary(lattice.getVelocity()[:,:,1], self._border_density_outlet)
            lattice_post[channels, :, -1] = outlet_eq[channels, :] + (lattice_pre[channels, :, 1] - lattice_eq[channels, :, 1])

        if self._orientation == self.Orientation.VERTICAL:
            #inlet
            channels = ci.bottom_channels()
            # column -2 so we have one additional column outside the grid
            inlet_eq = self.local_equilibrium_boundary(lattice.getVelocity()[:,-2,:], self._border_density_inlet)
            lattice_post[channels, 0, :] = inlet_eq[channels, :] + (lattice_pre[channels, -2, :] - lattice_eq[channels,-2, :])
            
            #outlet
            channels = ci.top_channels()
            outlet_eq = self.local_equilibrium_boundary(lattice.getVelocity()[:,1, :], self._border_density_outlet)
            lattice_post[channels, -1, :] = outlet_eq[channels, :] + (lattice_pre[channels, 1, :] - lattice_eq[channels, 1, :])