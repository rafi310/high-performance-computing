

import numpy as np
from mpi4py import MPI
import ipyparallel as ipp
from typing import Tuple
from numpy.lib.format import dtype_to_descr, magic


"""
needed:
Module for the parallel communication between the cores/the part of the grid
simple way to give a certain amount of cores to the lattice and it automatically 
split this up and set up the parallel communication between the grid parts

step 1: split global grid in multiple grids depending on the cores and grid shape
step 2: determine the area of the global grid where this process is responsible
step 3: create array with communication rows/columns and a communication buffer for the horizontal communication


"""

class ParallelGridManager():
    def __init__(self, X: int, Y:int) -> None:
        self._comm = MPI.COMM_WORLD
        
        self._size = self._comm.Get_size()
        self._rank = self._comm.Get_rank()
        self._splid_global_grid = self.getLocalGridSize(X,Y)
        self._local_grid_shape = self.getLocalShape(X, Y) # with the additional rows/columns for communication
        self._cartcomm= self._comm.Create_cart(dims=[*self._splid_global_grid],periods=[True,True],reorder=False)
        self._rcoords = self._cartcomm.Get_coords(self._rank)
        self._local_grid_pos = self._cartcomm.Get_coords(self._rank)
        self._global_X = X
        self._global_Y = Y
        self._xbuffer, self._ybuffer = self.computeBufferSize()
        self._valid_x, self._valid_y = self.computeValidSliceSize()
        self._sd = self.setUpCommunicationDirections()


    @property
    def local_grid_shape(self) -> Tuple[int, int]:
        """
        returns the grid shape of the lattice for this rank (plus additional communication layers)
        input: -
        return: Tuple[y size of grid; x size of grid]
        """
        return self._local_grid_shape

    @property
    def splid_global_grid(self) -> Tuple[int, int]:
        """
        returns the shape of the ranks that handle the global lattice
        input: -
        return: Tuple[rows of ranks; columns of ranks]
        """
        return self._splid_global_grid

    @property
    def local_buffer_shape(self) -> Tuple[int, int]:
        """
        returns the buffer shape for the communication between the ranks
        input: -
        return: Tuple[x buffer shape; y buffer shape]
        """
        return (self._xbuffer, self._ybuffer)

    @property
    def comm(self):
        """
        returns the global MPI commnication property 
        input: -
        return:  MPI communication property
        """
        return self._comm

    @property
    def cartcomm(self):
        """
        returns the communicator to which topology information has been attached
        input: -
        return: MPI communicator
        """
        return self._cartcomm
    
    @property
    def rcoords(self):
        """
        returns Coordinates of the rank in the global order
        input: -
        return: coordinates of the rank 
        """
        return self._rcoords
    
    @property
    def rank(self):
        """
        returns rank number
        """
        return self._rank
    
    @property
    def commDirections(self) -> np.array:
        """
        returns the information of the neighboring ranks 
        """
        return self._sd
    
    @property
    def valid_x(self) -> Tuple[int, int]:
        """
        get the valid x dimension without the additional communication layers
        """
        return self._valid_x

    @property
    def valid_y(self) -> Tuple[int, int]:
        """
        get the valid y dimension without the additional communication layers
        """
        return self._valid_y


    def startCluster(cores: int) :
        """
        just for jupyter
        """
        cluster = ipp.Cluster(engines="mpi", n=cores)
        client = cluster.start_and_connect_sync()
        client.ids


    def getLocalGridSize(self, NX, NY):
        """get the dimension in which the global grid is splid because of the core count
        2   00
        3   000
        4   00
            00
        5   -
        6   000
            000
        7   -
        8   0000
            0000
        9   000
            000
            000
        """
        sec_x, sec_y = 1, self._size
        for i in range(2, int(np.sqrt(self._size)) + 1):
            if self._size % i == 0:
                sec_x, sec_y = i, self._size / i
        assert sec_x > min(NX, NY) , f"{sec_x} are more rows than the grid size allows"
        assert sec_y > max(NX, NY) , f"{sec_y} are more columns than the grid size allows"
        return (sec_x, sec_y)
        
    def getLocalShape(self, X, Y) -> Tuple[int, int]:
        rows = self._splid_global_grid[0]
        columns = self._splid_global_grid[1]
        assert Y % rows == 0, f"{Y} rows is not evenly divisible by {rows}"
        assert X % columns == 0, f"{X} columns is not evenly divisible by {columns}"
        return (Y//columns+2, X//rows+2)

    def blockshaped(arr, nrows, ncols):
        """
        Return an array of shape (n, nrows, ncols) where
        n * nrows * ncols = arr.size

        The returned array should look like n subblocks with
        each subblock preserving the "physical" layout of arr.
        """
        h, w = arr.shape
        assert h % nrows == 0, f"{h} rows is not evenly divisible by {nrows}"
        assert w % ncols == 0, f"{w} cols is not evenly divisible by {ncols}"
        return (arr.reshape(h//nrows, nrows, -1, ncols)
                   .swapaxes(1,2)
                   .reshape(-1, nrows, ncols))

    def setUpCommunicationDirections(self) -> np.array:
        """
        define where to recive and where to send to
        
        0 source right shift
        1 destination right shift
        2 source left shift
        3 destination left shift
        4 top
        5 top
        6 bottom
        7 bottom
        """
        sR,dR = self._cartcomm.Shift(1,1)
        sL,dL = self._cartcomm.Shift(1,-1)
        sU,dU = self._cartcomm.Shift(0,1)
        sD,dD = self._cartcomm.Shift(0,-1)
        return np.array([sR,dR,sL,dL,sU,dU,sD,dD], dtype = int)
    

    def computeBufferSize(self) -> Tuple[int, int]:
        """
        get buffersize depending on position
        """
        bufx, bufy = self._local_grid_shape
        if self.isRightBorder():
            bufx -=1
        if self.isLeftBorder():
            bufx -=1
        if self.isTopBorder():
            bufy -=1
        if self.isBottomBorder():
            bufy -=1
        return (bufx, bufy)

    def computeValidSliceSize(self) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        """
        get the valid slice side without the additional communication layers
        output: Tuple[x start and end; y start and end]
        """
        valid_x_start, valid_y_start = 0,0
        valid_x_end, valid_y_end =  self.local_buffer_shape
        if not self.isRightBorder():
            valid_y_end -= 1
        if not self.isLeftBorder():
            valid_y_start +=1
        if not self.isTopBorder():
            valid_x_start +=1
        if not self.isBottomBorder():
            valid_x_end -= 1
        return ((valid_x_start, valid_x_end-1), (valid_y_start, valid_y_end-1)) #-1 to get the array index

    def isLeftBorder(self) -> bool:
        """
        return true if rank is the left border of the global lattice
        """
        is_border = False
        _ ,x_rank = self._rcoords
        if x_rank == 0:
            is_border = True
        return is_border
    
    def isRightBorder(self) -> bool:
        """
        return true if rank is the right border of the global lattice
        """
        is_border = False
        _, x_rank = self._rcoords
        _, x_max_rank  = self._splid_global_grid
        if x_rank == x_max_rank-1:
            is_border = True
        return is_border
    
    def isTopBorder(self) -> bool:
        """
        return true if rank is the top border of the global lattice
        """
        is_border = False
        y_rank, _ = self._rcoords
        if y_rank == 0:
            is_border = True
        return is_border
    
    def isBottomBorder(self) -> bool:
        """
        return true if rank is the bottom border of the global lattice
        """
        is_border = False
        y_rank, _ = self._rcoords
        y_max_rank, _ = self._splid_global_grid
        if y_rank == y_max_rank-1:
            is_border = True
        return is_border



    def save_mpiio(self, comm, fn, g_kl):
        """
        Write a global two-dimensional array to a single file in the npy format
        using MPI I/O: https://docs.scipy.org/doc/numpy/neps/npy-format.html

        Arrays written with this function can be read with numpy.load.

        Parameters
        ----------
        comm
            MPI communicator.
        fn : str
            File name.
        g_kl : array_like
            Portion of the array on this MPI processes. This needs to be a
            two-dimensional array.
        """
        magic_str = magic(1, 0)
        local_nx, local_ny = g_kl.shape
        nx = np.empty_like(local_nx)
        ny = np.empty_like(local_ny)
        commx = comm.Sub((True, False))
        commy = comm.Sub((False, True))
        commx.Allreduce(np.asarray(local_nx), nx)
        commy.Allreduce(np.asarray(local_ny), ny)
        arr_dict_str = str({ 'descr': dtype_to_descr(g_kl.dtype),
                             'fortran_order': False,
                             'shape': (np.asscalar(nx), np.asscalar(ny)) })
        while (len(arr_dict_str) + len(magic_str) + 2) % 16 != 15:
            arr_dict_str += ' '
        arr_dict_str += '\n'
        header_len = len(arr_dict_str) + len(magic_str) + 2
    
        offsetx = np.zeros_like(local_nx)
        commx.Exscan(np.asarray(ny*local_nx), offsetx)
        offsety = np.zeros_like(local_ny)
        commy.Exscan(np.asarray(local_ny), offsety)

        file = MPI.File.Open(comm, "log/mpiio/{}".format(fn), MPI.MODE_CREATE | MPI.MODE_WRONLY)
        if comm.Get_rank() == 0:
            file.Write(magic_str)
            file.Write(np.int16(len(arr_dict_str)))
            file.Write(arr_dict_str.encode('latin-1'))
        mpitype = MPI._typedict[g_kl.dtype.char]
        filetype = mpitype.Create_vector(g_kl.shape[0], g_kl.shape[1], ny)
        filetype.Commit()
        file.Set_view(header_len + (offsety+offsetx)*mpitype.Get_size(),
                      filetype=filetype)
        file.Write_all(g_kl.copy())
        filetype.Free()
        file.Close()