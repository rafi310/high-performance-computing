from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
import re
from typing import Optional
from matplotlib import animation
from IPython import display
from source.algorithms.BoundaryCondition import BoundaryHandlings
from source.algorithms.ParallelCommunication import ParallelGridManager
import source.data.CommonDefinitions as cd
from source.plotting.visualization import DensityPlot
import sys

"""
My Lattice Boltzmann Simulation 
Raphael Schneider
"""

class LatticeBoltzmannMethod():
    """
    global class which handles the lattice size and the Boltzman transport equation
    """
    def __init__(self, X: int, Y: int, omega: float,
                 init_lattice: Optional[np.ndarray] = None,
                 init_density: Optional[np.ndarray] = None,
                 init_velocity: Optional[np.ndarray] = None,
                 init_plot: bool = True,
                 parallel_comm: Optional[ParallelGridManager] = None):

        assert 0 < omega < 2
        self._is_parallel = False
        if parallel_comm is not None:
            self._is_parallel = True
            # get buffer shape -> for this shape this rank has to compute all necessary
            X, Y = parallel_comm.local_buffer_shape
        self._paralell_comm_manager = parallel_comm
        # set up lattice with given parameters
        self._lattice_cij = np.zeros((cd.NL, Y, X))
        self._lattice_cij_pre = np.zeros((cd.NL, Y, X))
        self._lattice_eq_cij = np.zeros((cd.NL, Y, X))
        self._density_ij = np.zeros((Y, X))
        self._velocity_aij = np.zeros((2, Y, X))
        self.grid_shape = (Y, X)
        self._mass = 0
        self._omega = omega
        self._X = X
        self._Y = Y
        if(init_plot):
            self._figure, self._ax = plt.subplots()
            self._do_plot = True
            self._title = self._ax.text(0.5, -0.05, "",
                                    size=plt.rcParams["axes.titlesize"],
                                    ha="center", va="top",
                                    transform=self._ax.transAxes)
            self._max_density = np.max(np.sum(self._lattice_cij, axis=0))
            self._min_density = np.min(np.sum(self._lattice_cij, axis=0))
            self._plot = DensityPlot(X, Y, plot=False, animate=False, vmin=self._min_density, vmax= self._max_density )
            self._plot.init(0, self._density_ij)
        else:
            self._do_plot = False
        self._images = []
        self._stream = []

        self._init_data(init_lattice_cij=init_lattice,
                        init_density_ij=init_density,
                        init_velocity_aij=init_velocity)


    def execute(self, timesteps: int, callback: Optional[int] = None, boundary: Optional[BoundaryHandlings] = None) -> None:
        """
        main execution method of the lattice boltzmann simulation
        input: 
            timesteps: timesteps how many simulation steps should be executed
            callback: callback function to log data for visualisation
            boundary: different boundary conditions
        """
        for i in range(timesteps):
            if i%10 == 0:
                sys.stdout.write("\r%d%%" %int(i/timesteps *100+1))
                sys.stdout.flush()        
                # print(i)
            self.boltzman_step(boundary=boundary)
            if callback is not None:
                callback(self, i)
            if self._do_plot:
                self.update_figure(step=i)
            
           

    def _init_data(self, init_lattice_cij: Optional[np.ndarray] = None,
                   init_density_ij: Optional[np.ndarray] = None,
                   init_velocity_aij: Optional[np.ndarray] = None):
        """
        init fields (density, velocity, and/or whole lattice) if they were predefined
        """
        if init_lattice_cij is not None:
            assert self._lattice_cij.shape == init_lattice_cij.shape
            self._lattice_cij = init_lattice_cij
        if init_density_ij is not None:
            assert self._density_ij.shape == init_density_ij.shape
            self._density_ij = init_density_ij
            # self._lattice_cij[0,...] =  init_density_ij
        else: 
            self.update_density()
        if init_velocity_aij is not None:
            assert self._velocity_aij.shape == init_velocity_aij.shape
            self._velocity_aij = init_velocity_aij
        else:
            self.update_velocity()
        if init_lattice_cij is None and (init_density_ij is not None or init_velocity_aij is not None):
            self.local_equilibrium()
            self._lattice_cij = self._lattice_eq_cij
        self._mass = np.sum(self._density_ij)



    @property
    def lattice_eq_cij(self) -> np.ndarray:        
        """
        returns the actual lattice with the equilibrium calculation
        input: -
        return: lattice_eq_cij 3D with each channel-info at x-y-Position
        """
        return self._lattice_eq_cij

    @lattice_eq_cij.setter
    def lattice_eq_cij(self) -> None:
        raise NotImplementedError("_lattice_eq_cij is not allowed to set from outside.")
    
    @property
    def lattice_cij_pre(self) -> np.ndarray:
        """
        returns the actual lattice at the state before the streaming (for boundary condition)
        input: -
        return: lattice_cij_pre 3D with each channel-info at x-y-Position
        """
        return self._lattice_cij_pre

    @lattice_cij_pre.setter
    def lattice_cij_pre(self) -> None:
        raise NotImplementedError("lattice_cij_pre is not allowed to set from outside.")
   
    @property
    def mass(self) -> float:
        """
        return total mass of lattice
        input: -

        """
        return self._mass

    def getDensity(self) -> np.ndarray:
        """
        returns the actual density array of the lattice
        input: -
        return: density_ij 2D with each density at x-y-Position
        """
        return self._density_ij

    def getVelocity(self) -> np.ndarray:
        """
        returns the actual velocity array of the lattice
        input: -
        return: velocity_aij 3D with each velocity at x-y-Position in x or y direction
        """
        return self._velocity_aij
        

    def getLattice(self) -> np.ndarray:
        """
        returns the actual lattice
        input: -
        return: lattice_cij 3D with each channel-info at x-y-Position
        """
        return self._lattice_cij
    
    def setLattice(self, lattice: np.ndarray) -> np.ndarray:
        """
        sets the actual lattice
        input: lattice_cij 3D with each channel-info at x-y-Position
        return: -
        """
        raise NotImplementedError("lattice_cij_pre is not allowed to set from outside.")

    def getGridShape(self): 
        """
        returns grid shape ((Y, X))
        """
        return self.grid_shape

    def streaming_part(self):
        """
	    streaming part of the lattice boltzmann equation
        update the main streaming lattice
	    """
        # iterate all channels for the shifts
        for idx in np.arange(cd.NL):
            self._lattice_cij[idx] = np.roll(
                self._lattice_cij[idx], cd.velocities_ca[idx], axis=(0, 1))

    def collision_part(self):
        """
        collision part of the lattice boltzman equation
        update the main lattice with the equilibirum equation
        input: -
        output: -
        """
        self.update_density()
        self.update_velocity()
        self.local_equilibrium()
        self._lattice_cij = self._lattice_cij +self._omega* (self._lattice_eq_cij - self._lattice_cij) 

    def update_density(self) -> None:
        """
        update the density field with the actual lattice
        input: -
        output: -
        """
        self._density_ij = np.sum(self._lattice_cij, axis=0)

    def update_velocity(self):
        """
        update the velocity field with the actual lattice
        input: -
        output: -
        """
        self._velocity_aij = np.einsum("cij,ca->aij", self._lattice_cij, cd.velocities_ca) \
            / np.maximum(self._density_ij, 1e-12)
        # velocity x
        # self._velocity_cjk[0, :, :] = (np.sum(self._lattice_ajk[[1, 5, 8], :, :], axis=0) - np.sum(
        #     self._lattice_ajk[[3, 6, 7], :, :], axis=0)) / np.maximum(self._density_ij, 1e-12)
        # # velocity y
        # self._velocity_cjk[1, :, :] = (np.sum(self._lattice_ajk[[2, 5, 6], :, :], axis=0) - np.sum(
        #     self._lattice_ajk[[4, 7, 8], :, :], axis=0)) / np.maximum(self._density_ij, 1e-12)

    def local_equilibrium(self) -> None:
        """
        local equilibirum equation for the collision part
        input: -
        output: -
        """
        # (2, Y, X).T @ (9,2).T -> (X,Y,9).T
        velo_dot_set = (self._velocity_aij.T @ cd.velocities_ca.T).T
        velo_abs = np.linalg.norm(self._velocity_aij, axis=0) ** 2
        # (9,10,15) = (9,1,1) * (1,10,15) * (9, 10 ,15)
        self._lattice_eq_cij = cd.weights[..., np.newaxis, np.newaxis] * self._density_ij[np.newaxis, ...] *\
               (1. + 3.*velo_dot_set+4.5 * velo_dot_set**2 - 1.5*velo_abs[np.newaxis, ...])

    def simulate_streaming(self):
        """
        just simulate the streaming part for milestone 1
        """
        self.update_density()
        self.streaming_part()

    def boltzman_step(self,  boundary: Optional[BoundaryHandlings] = None):
        """
        actually execution of one lattice boltzman step
        input: 
            boundary: boundary conditions
        output:- 
        """
        self._mass = np.sum(self._density_ij)
        # print(abs( np.sum(self._density_ij)/self._mass ))
        # assert abs( np.sum(self._density_ij)/self._mass-1.0 ) < 0.01, f"{abs( np.sum(self._density_ij)/self._mass-1.0)}"
        # if self._mass != np.sum(self._density_ij):
        #     change = np.sum(self._density_ij)/self._mass
        #     print("Density mass changed by {} %".format(change))
        if self._is_parallel:
            self.CommunicateParallel()
            #synchronization
            self._paralell_comm_manager.comm.Barrier()
        self._lattice_cij_pre = deepcopy(self._lattice_cij) 
        if boundary is not None:
            #handle periodic boundary with pressure gradient
            boundary(self, pressure=True)       
        self._lattice_cij_pre = deepcopy(self._lattice_cij) 
        self.simulate_streaming() 
        if boundary is not None:
            # handle other boundary conditions
            boundary(self, pressure=False)
        self.collision_part()

    def init_plot(self):
        """
        init the plot as a heatmap where you can see the density field of the lattice
        """
        self._ax.clear()
        heatmap = self._ax.pcolor(self._density_ij, cmap=plt.cm.Blues)
        # put the major ticks at the middle of each cell
        self._ax.set_xticks(np.arange(self.grid_shape[1]) + 0.5, minor=False)
        self._ax.set_yticks(np.arange(self.grid_shape[0]) + 0.5, minor=False)
        self._ax.invert_yaxis()
        self._ax.xaxis.tick_top()

        self._ax.set_ylabel('axis 0, first  index')
        self._ax.set_title('axis 1, second index')

    def update_figure(self, step: int):
        """
        update the figure after each boltzman step
        """
        self._max_density = max(self._max_density, np.max(self._density_ij))
        self._min_density = min(self._min_density, np.min(self._density_ij))
        self._plot.update(step, self._density_ij)
        # update heatmap
        img = self._ax.pcolormesh(self._density_ij,
                                  vmin=max(self._min_density, 0),
                                  vmax=max(self._max_density, 0.01),
                                  cmap=plt.cm.Blues)
        #things for streamplot animation
        x, y = np.meshgrid(np.arange(self._X), np.arange(self._Y))
        # level = np.linalg.norm(np.dstack([self._velocity_aij[0, :,:], self._velocity_aij[1, :,:]]), axis=-1).T
        # stream = self._ax.streamplot(x, y, self._velocity_aij[0, :,:], self._velocity_aij[1, :,:], color=level)
        # update title
        title = f"step {step}" if step > 0 else "initial state"
        # self._title.set_text(title)
        # use _ax.text instead of _ax.set_title, because the title would not get animated
        title = self._ax.text(0.5, -0.05, title,
                              size=plt.rcParams["axes.titlesize"],
                              ha="center", va="top",
                              # transform=self._ax.transAxes,
                              animated=True)
        if step%20==0:
            self._images.append([img, title])
        # self._stream.append([stream, title])

    def get_animation(self, interval: float = 0.05) -> animation.Animation:
        """
        Get a ``matplotlib.animation.Animation`` that comprises all performed simulation steps.

        Parameters
        ----------
        interval : float
            The delay between frames in seconds.
        """
        self._title.set_text("")
        return animation.ArtistAnimation(self._figure,
                                         self._images,
                                         interval=interval*1000,
                                         blit=True,
                                         repeat=False)

    def display_animation(self, autoplay: bool = False, **kwargs):
        """
        Display an animation of all performed simulation steps.
        This function is only useful inside a Jupyter/IPython environment.
        Parameters
        ----------
        autoplay : bool
            Whether to inject some JavaScript that automatically starts playing the video upon displaying.
        **kwargs
            Arguments for ``get_animation()``.
        """
        html = self.get_animation(**kwargs).to_jshtml()
        if autoplay:
            # html contains: animation_object_with_hash = new Animation(...);
            # append:        animation_object_with_hash.play_animation();
            html = re.sub(
                r"(\n( +\w+) = new Animation\([^)]+\);?)", "\\1\n\\2.play_animation();", html)
        display.display(display.HTML(html))


    def CommunicateParallel(self) -> None:
        """
        function to communicate with all neighbour parts of the grid
        """
        comm_manager = self._paralell_comm_manager
        commDirections = self._paralell_comm_manager.commDirections
        recvbuf = [np.zeros_like(self._lattice_cij[:, 0, :]),
                   np.zeros_like(self._lattice_cij[:, :, 0])]
        # print("nach rechts kommunizieren")
        sendbuf = self._lattice_cij[:, :, -2].copy()
        self._paralell_comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf, recvbuf=recvbuf[1],
                                        dest=commDirections[1], source=commDirections[0] )
        #if it is border it dont recive anything
        if not comm_manager.isLeftBorder():
            self._lattice_cij[:,:, 0] = recvbuf[1]
        # print("nach links kommunizieren")
        sendbuf = self._lattice_cij[:, : , 1].copy()
        self._paralell_comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf, recvbuf=recvbuf[1],
                                         dest=commDirections[3],  source=commDirections[2] )
        #if it is border it dont recive anything
        if not comm_manager.isRightBorder():
            self._lattice_cij[:,:, -1] = recvbuf[1]
        # print("nach oben kommunizieren")            
        sendbuf = self._lattice_cij[:,1,:].copy()
        
        self._paralell_comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf, recvbuf=recvbuf[0],
                                         dest=commDirections[5], source=commDirections[4] )
        #if it is border it dont recive anything
        if not comm_manager.isBottomBorder():
            self._lattice_cij[:, -1,:] = recvbuf[0]
        # print("nach unten kommunizieren")
        sendbuf = self._lattice_cij[:, -2, : ].copy()
        self._paralell_comm_manager.cartcomm.Sendrecv(sendbuf=sendbuf, recvbuf=recvbuf[0],
                                         dest=commDirections[7], source=commDirections[6] )
        #if it is border it dont recive anything
        if not comm_manager.isTopBorder():
            self._lattice_cij[:, 0,:] = recvbuf[0]
