from tkinter import W
from source.algorithms.latticeBoltzman import LatticeBoltzmannMethod
import source.algorithms.BoundaryCondition as bc
from source.data.ExperimentData import ExperimentVar
import source.data.ExperimentData as ed
import source.data.CommonDefinitions as cd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib as mpl

# plt.rcParams['animation.ffmpeg_path'] ='C:\\Users\\Raphael\\ffmpeg\\bin'

def main2():
    X, Y = 100, 100
    density_field = np.ones((Y, X))
    c_d = min(X,Y)//4
    p_x, p_y = X//3,Y-c_d-1
    # density_field[p_y-c_d:p_y+c_d, p_x-c_d:p_x+c_d] = 0.3
    lattice_pdf = LatticeBoltzmannMethod(X, Y, omega=1 , init_density=density_field, init_plot=True)
    boundary_list =[]

    boundary_list.append(bc.RigidWall(boundaryAlignment=cd.BoundaryAlignments.BOTTOM, lattice=lattice_pdf))
    # boundary_list.append(bc.MovingWall(boundaryAlignment=cd.BoundaryAlignments.TOP, lattice=lattice_pdf, wall_velo=np.array([1,0])))
    boundary_list.append(bc.RigidWall(boundaryAlignment=cd.BoundaryAlignments.TOP, lattice=lattice_pdf))
    boundary_list.append(bc.PeriodicWithPressure(boundaryAlignment=cd.BoundaryAlignments.LEFT, boundaryAlignment2=cd.BoundaryAlignments.RIGHT,
     lattice=lattice_pdf, density_inlet=1.0, density_outlet=.9))
    boundary_handling = bc.BoundaryHandlings(boundary_conditions=boundary_list)
    
    lattice_pdf.execute(4000, boundary=boundary_handling)
def main():
    filename = "log/sliding lid/velocitiesRe1000U_x=0.5.npy"
    velo_data = np.load(filename)
    # print(velo_data[0])
    plt.style.use('classic')
    X,Y = 300, 300
    time_steps = 100000
    animation_timesteps = np.concatenate((np.arange(0,1000, 10),
                                         np.arange(1000, 5000, 100),
                                         np.arange(5000, time_steps, 1000)))


    iterator = range(len(animation_timesteps))
    idx = 200
    vy = velo_data[iterator[idx], 0, :, :]
    vx = velo_data[iterator[idx], 1, :, :]
    fig, ax = plt.subplots(figsize=(18,13))
    x, y = np.meshgrid(np.arange(X), np.arange(Y))
    cmap = "coolwarm"
    # ax.set_title("timestep {}".format(animation_timesteps[iterator[idx]]), size=14, y=-0.05)
    # plt.subplot()

    # stream = ax.streamplot(x, y, vx, vy, color=vx, density=2, cmap=cmap, arrowsize=1)

    color_norm = mpl.colors.Normalize(vmin=0.0, vmax=float(filename[-7:-4]))
    fig.tight_layout()
    p = ax.get_position()
    def animate(iter):
        ax.clear()
        fig.colorbar(mpl.cm.ScalarMappable(norm=color_norm, cmap=cmap), \
            ax=fig.add_axes([p.x0+0.05, p.y0,p.width, p.height], visible=False), label='velocity $u$', shrink=0.5)
        ax.plot(np.arange(-1, X+1), np.ones((Y+2))*-0.5, 'r-4', linewidth=2, label="moving $U_x={}$".format(filename[-7:-4]))
        ax.plot(np.arange(-1, X+1), np.ones((Y+2))*(Y+0.5), 'k', linewidth=2, label="rigid")
        ax.plot(np.ones((X))*-.5, range(Y), 'k', linewidth=2, )
        ax.plot(np.ones((X))*(X+.5), range(Y), 'k', linewidth=2)
        ax.set_xlim(-2, X+2)
        ax.set_ylim(-2, Y+2)
        ax.set_xlabel('$x$ dimension')
        ax.set_ylabel('$y$ dimension')
        ax.legend(title="boundary" ,bbox_to_anchor=(1.15, 1.0), prop={'size':12})
        ax.invert_yaxis()
        ax.xaxis.tick_top()
        ax.xaxis.set_label_position("top")
        # ax.collections = []
        # ax.patches = []
        ax.set_title("timestep {}".format(animation_timesteps[iterator[iter]]), size=14, y=-0.05)
        vy = velo_data[iterator[iter], 0, :, :]
        vx = velo_data[iterator[iter], 1, :, :]
        # ax.get_tightbbox()
        stream = ax.streamplot(x, y, vx, vy, color=vx, density=2, cmap=cmap, arrowsize=1)
        plt.tight_layout()  
        print(iter)
        return stream
    anim =   animation.FuncAnimation(fig, animate, frames=len(animation_timesteps), interval=50, blit=False, repeat=False)
    # anim =   animation.FuncAnimation(fig, animate, frames=10, interval=50, blit=False, repeat=False)
    # plt.show()
    f = r"animation.mp4" 
    writermp4 = animation.FFMpegWriter(fps=10) 
    anim.save("figures/Milestone6/slidingLidRe1000.gif")#, writer=writermp4)
if __name__ == "__main__":
    main()
