from enum import Enum
from enum import IntEnum
import numpy as np
class BoundaryAlignments(Enum):
    RIGHT = 0,
    TOP = 1,
    LEFT  = 2,
    BOTTOM = 3


velocities_ca = np.array([
            # [Y, X] shift
            [0, 0],    # channel 0: center
            [0, 1],    # channel 1: right / east
            [-1, 0],   # channel 2: top / north
            [0, -1],   # channel 3: left / west
            [1, 0],    # channel 4: bottom / south
            [-1, 1],   # channel 5: top-right / north-east
            [-1, -1],  # channel 6: top-left / north-west
            [1, -1],   # channel 7: bottom-left / south-west
            [1, 1]     # channel 8: bottom-right / south-east
        ])
# equilibrium occupation numbers
weights = np.array([4/9, 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36])
NL = 9  # channel count


class ChannelDirections(IntEnum):
    """ Enum from all 9 2D-Channels"""
    CENTER = 0,
    RIGHT = 1,
    TOP = 2,
    LEFT  = 3,
    BOTTOM = 4,
    TOPRIGHT = 5,
    TOPLEFT = 6,
    BOTTOMLEFT = 7,
    BOTTOMRIGHT = 8,

class ChannelDirectionsReflected(IntEnum):
    """ Enum from all 9 2D-Channels"""
    CENTER =      ChannelDirections.CENTER,
    RIGHT =       ChannelDirections.LEFT,
    TOP =         ChannelDirections.BOTTOM,
    LEFT  =       ChannelDirections.RIGHT,
    BOTTOM =      ChannelDirections.LEFT,
    TOPRIGHT =    ChannelDirections.BOTTOMLEFT,
    TOPLEFT =     ChannelDirections.BOTTOMRIGHT,
    BOTTOMLEFT =  ChannelDirections.TOPRIGHT,
    BOTTOMRIGHT = ChannelDirections.TOPLEFT,

class ChannelInformation():
    def __init__(self) -> None:
        self._channelAssistant = {
        # get lattice row, column, channel, rev. channel
        BoundaryAlignments.RIGHT: (None, -1, right_channels(), left_channels()),
        BoundaryAlignments.TOP: (0, None, top_channels(), bottom_channels()),
        BoundaryAlignments.LEFT: (None, 0, left_channels(), right_channels()),
        BoundaryAlignments.BOTTOM: (-1, None, bottom_channels(), top_channels())
    }

    def left_channels() -> np.array:
        return np.array([ChannelDirections.LEFT, ChannelDirections.TOPLEFT, ChannelDirections.BOTTOMLEFT])
    def right_channels() -> np.array:
        return np.array([ChannelDirections.RIGHT, ChannelDirections.BOTTOMRIGHT, ChannelDirections.TOPRIGHT])
    def top_channels() -> np.array:
        return np.array([ChannelDirections.TOP, ChannelDirections.TOPLEFT, ChannelDirections.TOPRIGHT])
    def bottom_channels() -> np.array:
        return np.array([ChannelDirections.BOTTOM, ChannelDirections.BOTTOMRIGHT, ChannelDirections.BOTTOMLEFT])

    def getChannelReflected(self, boundary: BoundaryAlignments) -> np.array:
        if boundary == BoundaryAlignments.TOP:
            return self.bottom_channels()
        elif boundary == BoundaryAlignments.BOTTOM:
            return self.top_channels()
        elif boundary == BoundaryAlignments.RIGHT:
            return self.left_channels()
        elif boundary == BoundaryAlignments.LEFT:
            return self.right_channels()

    
    @property
    def channelAssistant(self): 
        """
        assistant to map the boundary directions to the row/columns and channels
        --> lattice row, column, channel, rev. channel
        """
        return self._channelAssistant

def get_viscosity(omega: float):
    """
    calculate viscosity for a given omega
    input: omega
    output: viscosity
    """
    visc = 1. / 3. * (1. / omega - 0.5)
    return visc

def get_omega_from_visc(visc: float) -> float:
    """
    calculate omega from a given viscosity
    input: viscosity
    output: omega
    """
    omega = 1. /(visc *3. +0.5)
    return omega

def calc_raynoldsnumber(omega: float, velocity: float, length: int) -> int:
    """
    calculate the raynolds number in a quadratic grid
    input: omega, wall velocity, length of the quadratic grid
    output: raynolds number (int)
    """
    visc = get_viscosity(omega=omega)
    return int(length * velocity/visc) 

def get_omega_for_raynoldnumb(raynold: int, velocity: float, length: int) -> float:
    """
    calculate omega for given parameters 
    input: reynoldsnumber, wall velocity, channel length
    ouput: omega 
    """
    visc = (velocity*length)/raynold
    return get_omega_from_visc(visc)