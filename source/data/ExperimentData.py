

from matplotlib.pyplot import grid
import numpy as np
from typing import Tuple


class ExperimentVar():
    def __init__(self, omega: float = 1.0,
                 epsilon: float = 0.05,
                 total_time_steps: int = 10000,
                 lattice_shape: Tuple[int, int] = (100, 100), # X,Y
                 rho: float = 0.8,) -> None:
        self._omega = omega
        self._epsilon = epsilon
        self._total_time_steps = total_time_steps
        self._lattice_shape: Tuple[int, int] = lattice_shape
        self._rho: float = rho
    @property
    def omega(self) -> float:
        return self._omega
    @property
    def epsilon(self) -> float:
        return self._epsilon
    @property
    def time_steps(self) -> int:
        return self._total_time_steps
    @property
    def lattice_shape(self) -> Tuple[int, int]:
        return self._lattice_shape
    @lattice_shape.setter
    def lattice_shape(self, value) :
        self._lattice_shape = value
    @property
    def rho(self) -> float:
        return self._rho

def sinus_density(grid_shape: Tuple[int, int], epsilon: float, rho_zero: float) -> Tuple[np.ndarray, np.ndarray]:
    """
    make sinusoidal density distribution
    ρ(r,0)=ρ0+εsin(2πx/L_x)  and u(r,0)=0
    input: lattice shape, epsilon of sinusoidal, rho 
    output: 2D lattice with sinusoidal density distribution along the row 
    """ 
    assert 0 < rho_zero <= 1 
    assert rho_zero-epsilon >= 0
    X, Y = grid_shape
    #velocity field
    velocity = np.zeros((2, Y, X))
    #density field
    density_row = rho_zero + epsilon * np.sin(2*np.pi*np.arange(X) / X)
    density = np.tile(density_row, (Y, 1))
    return density, velocity


def sinus_velocity(grid_shape: Tuple[int, int], epsilon: float)  -> Tuple[np.ndarray, np.ndarray]: 
    """
    make sinusoidal velocity distribution along x-axis
    ρ(r,0)=1  and  ux(r,0)=εsin(2πy/L_y)
    input: lattice shape, epsilon of sinusoidal
    output: 2D lattice with sinusoidal velocity distribution 
    """
    assert abs(epsilon) < 0.1
    X,Y = grid_shape
    #density field
    density = np.ones((Y, X))
    #velocity field
    velocity = np.zeros((2, Y, X))
    velocity_column = epsilon * np.sin(2*np.pi*np.arange(Y) / Y)
    # velocity[0,...] = np.tile(velocity_column, (X,1))
    velocity = np.array([np.zeros((Y,X)), np.tile(velocity_column[:, np.newaxis], (1,X))])
    return density, velocity


def dens1_velo0(grid_shape: Tuple[int, int])  -> Tuple[np.ndarray, np.ndarray]:
    """
    init density field with one and velocity field with zero
    input: lattice shape
    output: density field, velocity field
    """
    X,Y = grid_shape
    #density field
    density = np.ones((Y, X))
    #velocity field
    velocity = np.zeros((2, Y, X))
    return density, velocity