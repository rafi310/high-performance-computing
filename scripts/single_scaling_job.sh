#!/bin/bash -x
module load devel/python/3.10.0_gnu_11.1
module load compiler/gnu/12.1
module load mpi/openmpi/4.1
module list
pip install --user --upgrade numpy matplotlib mpi4py ipyparallel

while getopts ":N:" o
do
    case "${o}" in
        N) N=${OPTARG};;
    esac
done

echo "Running on ${SLURM_JOB_NUM_NODES} nodes with ${SLURM_JOB_CPUS_PER_NODE} cores each."
echo "Use ${N} threads."
echo "$PWD"
#mpirun -n $N python -m hpc/run_sliding_lid_exp -X 300 -Y 300 -T 10000 -RE 750 -P True
mpirun -n $N python hpc/run_sliding_lid_exp.py -X 300 -Y 300 -T 100000 -RE 750 -P True