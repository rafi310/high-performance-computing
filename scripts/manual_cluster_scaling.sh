#!/bin/bash
#SBATCH --nodes=2
#SBATCH --time=00:15:00
#SBATCH --partition=dev_multiple
#SBATCH --ntasks-per-node=25
#SBATCH -J HPC_WITH_PYTHON
#SBATCH --output=slurm.out
#SBATCH --error=slurm.err
echo "Loading Pythona module and mpi module"
module load devel/python/3.10.0_gnu_11.1
module load compiler/gnu/12.1
module load mpi/openmpi/4.1
module list
pip install --user --upgrade numpy matplotlib mpi4py ipyparallel
echo "Running on ${SLURM_JOB_NUM_NODES} nodes with ${SLURM_JOB_CPUS_PER_NODE} cores each."
echo "Each node has ${SLURM_MEM_PER_NODE} of memory allocated to this job."
time mpirun python hpc/run_sliding_lid_exp -X 300 -Y 300 -T 10000 -RE 750 -P True

# startexe="mpirun --bind-to core --map-by core -report-bindings python3 ./ProcessorTest.py"
# exec $startexe
