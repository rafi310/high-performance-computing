
echo "Loading Pythona module and mpi module"
module load devel/python/3.10.0_gnu_11.1
module load compiler/gnu/12.1
module load mpi/openmpi/4.1
module list
pip install --user --upgrade numpy matplotlib mpi4py ipyparallel
N_THREADS_LIST=(625 900 1100 1200 1300 1400 1500 1600 1700 1800 2500)
#N_THREADS_LIST=(1 2 3 4 6 8 16 25 30 36 60 90 100 128 200 300 400 500 )
#N_THREADS_LIST=(200 300 350 400 450 500)
for N_THREADS in "${N_THREADS_LIST[@]}"
do
    NODES=$((${N_THREADS}/40+1))
    if [ $NODES == 1 ]
    then 
      ((NODES = NODES + 1))
    fi
    echo Use ${N_THREADS} threads on ${NODES} nodes.
    echo sbatch --nodes=${NODES} --time=00:45:00 --partition=multiple --ntasks-per-node=40 -J HPC_WITH_PYTHON --output=slurm$N_THREADS.out --error=slurm.err hpc/scripts/single_scaling_job.sh -N $N_THREADS
    sbatch --nodes=${NODES} --time=00:45:00 -p multiple --ntasks-per-node=40 -J HPC_WITH_PYTHON --output=slurm$N_THREADS.out --error=slurm.err hpc/scripts/single_scaling_job.sh -N $N_THREADS
    #mpirun -n $N_THREADS python -m run_sliding_lid_exp-X 300 -Y 300 -T 10000 -RE 750 -P True
    #echo "$PWD"
    #mpirun -n $N_THREADS python -m run_sliding_lid_exp -X 300 -Y 300 -T 10000 -RE 750 -P True
    echo ""
done

